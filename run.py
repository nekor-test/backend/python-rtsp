"""An example script to use with flake8."""


def hello(msg: str) -> None:
    """Greeting humans."""
    print(msg)


if __name__ == '__main__':
    hello('Hello, world!')
